# README #

SimpleSyndicate.Mvc.EditorTemplates NuGet package.

Get the package from https://www.nuget.org/packages/SimpleSyndicate.Mvc.EditorTemplates

### What is this repository for? ###

Common editor templates for ASP.Net MVC applications:

* Nullable<DateTime>
* int?

### How do I get set up? ###

* Intall the NuGet package and the editor templates will be copied into your project.

### Reporting issues ###

* Use the tracker at https://bitbucket.org/gazooka_g72/simplesyndicate.mvc.editortemplates/issues?status=new&status=open
