﻿SimpleSyndicate.Mvc.EditorTemplates NuGet package.

Common editor templates for ASP.Net MVC applications:
- Nullable<DateTime>
- int?

The editor templates have been copied to your Views\Shared\EditorTemplates folder.
